# THIS DOC IS DEPRECATED

### Disclaimer

( THIS CONTAINS VERY APPROXIMATIVE INFORMATION, CHECK README.md FOR MORE A TECHNICAL DESCRIPTION )
( THE DESCRIPTION IN THIS PAPER IS NOT TO BE TAKEN AS DEFINITIVE AS IT'S ONLY PURPOSE IS TO GIVE A
 VERY QUICK AND SIMPLE INTRODUCTION TO THE PROJECT )

## The repository

The repository works also as a process journal for the project, you can check `Commits` to
see all the changes, when they have been done and why.

# Material Recognition

The goal of this project is to create a device that can detect the material
of objects using machine learning, processing visual inputs and inputs
from capacitive sensors.

Here are **Video Explanations and Demo** :

- Demo : https://drive.google.com/file/d/161m0ODIkti9JZta7VPXPM_vh3MUH7wG0/view?usp=sharing

- Videos ( very informal and long :) ):
    https://drive.google.com/file/d/1aoYc9qeSaHRnK0GntXA-wrNn0K1YFNdO/view?usp=sharing
    https://drive.google.com/file/d/1Giz2kzqqIYhajRz5NUagJ6Y6Pm66g4q_/view?usp=sharing

## Visual Recognition

So far I've built a working beta version of the algorithm, it is able to give a
prediction of the material of an object given as an input only an image.

To do this I've used a type of Neural Network (https://en.wikipedia.org/wiki/Artificial_neural_network)
called convolutional neural network which is usually used for solving non linear
problems like the one I'm talking about.
Given the premise that you know what a neural network is I will explain the concept
of a convolutional neural network.

A ConvNet is a very common machine learning technique used when a complex input is given, like an image.
A ConvNet has mainly 2 different stages from a normal neural network :

- **Convolution** : A convolution is a data processing technique used to reduce the
complexity of a given input. In our case our input is an image, an image computationally
is a 3D matrix, basically think about it as three 2D grids all stacked together, this because
there are 3 channels usually in an image which are RGB, so each grid contains intensity values,
one for Red one for Green and the other for Blue. As you can see an image is a pretty complex
input for a normal neural network, so to do this we use convolution. In short convolution analyses
the image using something called kernel, which is a small window of pixels that scans through the
image and collects the more important (higher) values that it finds in order to reduce the size of the
input and extract the most important parts.

- **Pooling** : Pooling is used after each convolution to further skim the image and detect what are so
called features of the images which are basically the things our algorithm will be looking for. The type of
pooling technique I used is MaxPooling which only considers the highest values as important, there is also is
MeanPooling and others.

Nevertheless these two technique aren't enough for classification, there is also the need of a "classical
neural network" that actually does the learning.
Assuming you already know what a neural network is, you know that these type of **models** learn off examples.
In order to create a functioning model that performs decently there is the need of thousands of samples
that the algorithm can use to learn, I didn't have the luxury to have that much data at first but then I realised
that the only thing my algorithm had to learn was how the **texture** of each material looked like.
This allowed me to instead of having to find thousands of images to instead collect around 100 and
sample them by taking **40x40** pixels area of the image to train my model. This allowed me to have **2000 samples**
for each category.

So far the categories I trained my model to recognize are Plastic, Paper and Glass. But I'm planning to add
metals as well, but they will probably be classified using sensors and not visual input. (or both)

## Capacitive Sensors

This is the second part of the classification, using capacitive sensors I will make the classification
even more precise.
"*In electrical engineering, capacitive sensing (sometimes capacitance sensing) is a technology, based on
capacitive coupling, that can detect and measure anything that is conductive or has a dielectric different from air.*"
This is the definition of the capacitive principle, it is actually simpler than it sounds and here is how I will use it:

- A capacitive sensor creates a magnetic field using a conducting material and a resistor

- When a body enters that magnetic field (like touching the sensor) the current flowing between
the circuit will change

- Different materials differ in how they interact with the magnetic field, making it change in different ways

- All I have to do is measure the change in the magnetic field and associate each value to a different material

Here is the circuit schematic I will be using :
![Schematic](https://s8.postimg.cc/uxxhfq1fp/schematic2.png)

## Physical and Hardware

In the future I will discuss about :

- The design

- The functioning of the hardware

- The user interaction

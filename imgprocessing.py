import os
import cv2
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt

DIMENSIONS = [512, 384]

class Chunker(object):
    def __init__(self, chunk, trainer):
        self.chunk = chunk
        self.total = self.chunk*self.chunk
        self.trainer = trainer

    def single(self, path):
        img = cv2.imread(path)
        chunkes = []
        #Check and fix image size
        height, width = img.shape[:2]
        if self.trainer and (height < DIMENSIONS[1] or width < DIMENSIONS[0]):
            print("Image is too small! Has to be : {}x{}".format(DIMENSIONS[0], DIMENSIONS[1]))
            return chunkes

        img = cv2.resize(img, (DIMENSIONS[0]-(DIMENSIONS[0]%self.chunk),
                                DIMENSIONS[1]-(DIMENSIONS[1]%self.chunk)))

        height, width = img.shape[:2]
        #Remove background
        back = np.zeros((1,65), np.float64)
        front = np.zeros((1,65), np.float64)
        rect = (30, 30, 500, 500)
        mask = np.zeros(img.shape[:2], np.uint8)
        cv2.grabCut(img, mask, rect, back, front, 5, cv2.GC_INIT_WITH_RECT)
        mask2 = np.where((mask==2) | (mask==0), 0, 1).astype('uint8')
        img = img*mask2[:,:,np.newaxis]
        #Window size for sampling
        coord = [self.chunk, self.chunk]
        while True:
            square = img[coord[1]-self.chunk:coord[1], coord[0]-self.chunk:coord[0]]
            #Calculate percentage of black
            gray = cv2.cvtColor(square, cv2.COLOR_BGR2GRAY)
            if self.total-cv2.countNonZero(gray) < self.total/2:
                chunkes.append(square)

            if coord[0] < width:
                coord[0] += self.chunk
            elif coord[0] >= width and coord[1] < height:
                coord[0] = self.chunk
                coord[1] += self.chunk
            else:
                break

        return chunkes

    def toPieces(self, data, out, amount):
        self.data = data
        self.out = out

        if amount == 0:
            amount = len(os.listdir(self.data))

        count = [1, 1]
        for image in tqdm(os.listdir(self.data)):
            try:
                count[1] = 1
                if count[0] >= amount:
                    break
                path = os.path.join(self.data, image)
                if os.path.isdir(path):
                    continue
                chunkes = self.single(path)
                for sample in chunkes:
                    cv2.imwrite(self.out+str(count[0])+"-"+str(count[1])+".jpg", sample)
                    count[1] += 1

                count[0] += 1

            except:
                continue

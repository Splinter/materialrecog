# THIS DOC IS DEPRECATED

![License](https://img.shields.io/badge/license-GNU%20AGPLv3-blue.svg)

( THIS DOC CONTAINS TECHNICAL STUFF, FOR SIMPLE EXPLANATION AND DEMO USE `INTRO.MD` )

# Material Recognition

A project which aims to create a IOT device that can detect the material of an
object for recycling purposes.

## So far

So far I handled the data processing for the visual classification, since there
was no dataset I didn't have a lot of data to base the Convolutional Neural Network
I was planning to do. To solve this I decided to do some advanced image processing
to increase the data I was working with. To do so I :

- Remove the background of the sample
- Resize the images so that they would be divisible by a fixed window size
- Cut in chunks the image (40px)
- Remove non-useful images (monocolor/no-edges)

And from 78 images per class (paper, glass, plastic) I have 2000+ samples to work with
to create an algorithm that is more "texture detection" using more low level features.
The input of the CNN will be one single chunk of image, so the actual input data will
be a bunch of these chunks and a total percentage of the image will be created.

### CNN Graph Structure

![Graph](https://s8.postimg.cc/i44r9yfut/graph.png)

- Input Layer (40x40x3)
- Conv2D Layer (10x10x16)
- MaxPool2D Layer (4x4, 1x1)
- Conv2D Layer (5x5x32)
- MaxPool2D Layer (2x2)
- Conv2D Layer (2x2x64)
- MaxPool2D Layer (2x2)
- Conv2D Layer (2x2x64)
- MaxPool2D Layer (2x2)
- Conv2D Layer (2x2x128)
- MaxPool2D Layer (2x2)
- Conv2D Layer (2x2x128)
- MaxPool2D Layer (2x2)
- Fully Connected Layer (2048)
- Dropout (0.5)
- Fully Connected Layer (1024)
- Dropout (0.5)
- Fully Connected Layer (3)
- Softmax

I rand 10 different versions with different connections and this is the one that seems to
work the best for now, all ran 300 epochs.

## Later

I will also add inductive and capacitive sensors to make the classification more accurate
and also include metal detection.
I will have to decide the distance of the camera from the object to detect and all the measures
and equipment needed.
I will also have to think about collecting more data and maybe add metal to visual recognition too
and create a system that understands when the object isn't part of any class.
So I will have to think about retraining.
The model I created is pretty solid I think and I will consider using only Tensorflow or from scratch too

## Dataset Created

Here is the link for the dataset I created from images I collected, also a version of it after using
`imgprocessing.py` - https://goo.gl/TZSYkZ

The models will not be shared at the moment.

# Created by Tobia Righi :satellite:

# LICENSE

GNU AGPLv3 :

  Permissions of this strongest copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. When a modified version is used to provide a service over a network, the complete source code of the modified version must be made available.

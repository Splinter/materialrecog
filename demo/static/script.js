Webcam.set({
  width: 480,
  height: 480,
  image_format: 'jpeg',
  jpeg_quality: 100,
  force_flash: false,
  flip_horiz: true,
  fps: 45
});
Webcam.attach('#camContainer');
function take_snapshot() {
  Webcam.snap(function(data_uri) {
    Webcam.upload(data_uri, '/', function (code, text) {
      if (code == 200){
        result = JSON.parse(text);
        console.log(result);
      }else{
        alert("Error processing image! Reloading page");
        location.reload();
      }
    });
  });
}

function uploadFile() {
  $.ajax({
    url: '/',
    type: 'POST',
    data: new FormData($('form')[0]),
    cache: false,
    contentType: false,
    processData: false,
    xhr: function() {
      var myXhr = $.ajaxSettings.xhr();
      return myXhr;
    },
    success: function (response) {
      console.log(response);
    }
  });
}

package views

import (
	"../settings"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

//A struct for the context in the page
type Context struct {
	Title     string
	StaticDir string
}

type Result struct {
	Error   string `json:"error"`
	Image   string `json:"image"`
	Final   string `json:"final"`
	Plastic string `json:"plastic"`
	Glass   string `json:"glass"`
	Paper   string `json:"paper"`
}

//Prepare for settings
var confs settings.Setting

var allowed = [3]string{"jpg", "jpeg", "png"}

//Index : the main page
func Index(w http.ResponseWriter, r *http.Request) {
	//Load settings
	confs = settings.Load()
	if r.Method == "GET" {
		//Add token to the tokens
		context := Context{
			Title:     "Material Recognition - Demo",
			StaticDir: confs.StaticDir,
		}
		t, _ := template.ParseFiles("templates/index.html")
		t.Execute(w, context)
	} else if r.Method == "POST" {
		r.ParseMultipartForm(32 << 20)
		var result Result
		//Load the file
		file, header, err := r.FormFile("webcam")
		if err != nil {
			fmt.Println(err)
			result.Error = "Error getting the image! Try again."
			sendResult(result, w)
			return
		}
		if !validImage(header) {
			result.Error = "Not a valid image! Must be jpg, jpeg or png!"
			sendResult(result, w)
			return
		}
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 12))
		fileName := fmt.Sprintf("%x.jpg", h.Sum(nil))
		f, err := os.OpenFile("images/"+fileName, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
			result.Error = "Error getting the image! Try again."
			sendResult(result, w)
			return
		}
		defer f.Close()
		io.Copy(f, file)

		/* NETWORK JOB */

		result.Error = ""
		result.Image = "/images/" + fileName
		result.Final = "Plastic"
		result.Plastic = "80.5"
		result.Glass = "5.5"
		result.Paper = "14.0"
		sendResult(result, w)
		go removeImage("images/" + fileName)
	} else {
		http.Redirect(w, r, confs.Home, http.StatusMovedPermanently)
	}
}

func validImage(header *multipart.FileHeader) bool {
	tmp := strings.Split(header.Filename, ".")
	ext := tmp[len(tmp)-1]
	tmp = strings.Split(header.Header["Content-Type"][0], "/")
	m := tmp[len(tmp)-1]
	valid := false
	for _, a := range allowed {
		if m == a {
			valid = true
		}
	}
	if valid {
		valid = false
		for _, a := range allowed {
			if ext == a {
				valid = true
			}
		}
	}
	return valid
}

func sendResult(result Result, w http.ResponseWriter) {
	b, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(b))
}

func removeImage(image string) {
	time.Sleep(time.Minute)
	os.Remove(image)
}

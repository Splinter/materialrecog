import os
import cv2
import time
import pickle
import random
import threading
from imgprocessing import Chunker

import sys
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')

import tflearn
import numpy as np
from tqdm import tqdm
import tensorflow as tf
from tflearn.layers.estimator import regression
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected

sys.stderr = stderr

IMAGE_SIZE = [40, 40]
LABELS = {
    "plastic":[1,0,0],
    "paper":[0,1,0],
    "glass":[0,0,1],
}
DATA_USED = 2535
TRAIN_DATA = 2000
DATA_FILE = "data.npy"
DATA_DIR = "/home/paperspace/Desktop/MaterialRecognition/data/processed/"
TEST_IMAGE = '/home/paperspace/Desktop/MaterialRecognition/data/bottle.jpg'
TESTERS = '/home/splinter/MaterialRecognition/data/testers/'
ITERATION = 4
MODEL = "materials-{}".format(ITERATION)
LIGHT_MODEL = MODEL+"-light"
DO_TRAIN = False

def dataSetup(src):
    data = []
    for label in LABELS:
        path = os.path.join(src, label)
        count = 0
        for image in tqdm(os.listdir(path)):
            if count >= DATA_USED:
                break
            count += 1
            img = cv2.imread(os.path.join(path, image))
            data.append([np.array(img), np.array(LABELS[label])])

    random.shuffle(data)
    np.save(DATA_FILE, data)
    return data

class CNN(object):
    def __init__(self, name, epoch):
        self.name = name
        self.epoch = epoch
        self.model = None

    def create(self):
        #Input layer by IMAGE_SIZExIMAGE_SIZEx3 (3 for the rgb channels)
        self.network = input_data(shape=[None, IMAGE_SIZE[0], IMAGE_SIZE[1], 3], name="input")
        self.input_layer = self.network
        #Convolutions, ReLU and MaxPooling Layers
        self.network = conv_2d(self.network, 16, 10, activation="relu", name="Conv1-10x10x16")
        self.network = max_pool_2d(self.network, 4, name="Pool1-4x4")
        self.network = conv_2d(self.network, 32, 5, activation="relu", name="Conv2-5x5x32")
        self.network = max_pool_2d(self.network, 2, name="Pool2-2x2")
        self.network = conv_2d(self.network, 64, 2, activation="relu", name="Conv3-2x2x64")
        self.network = max_pool_2d(self.network, 2, name="Pool3-2x2")
        self.network = conv_2d(self.network, 64, 2, activation="relu", name="Conv4-2x2x64")
        self.network = max_pool_2d(self.network, 2, name="Pool4-2x2")
        self.network = conv_2d(self.network, 128, 2, activation="relu", name="Conv5-2x2x32")
        self.network = max_pool_2d(self.network, 2, name="Pool5-2x2")
        self.network = conv_2d(self.network, 128, 2, activation="relu", name="Conv6-2x2x64")
        self.network = max_pool_2d(self.network, 2, name="Pool6-2x2")
        #Fully Connected Layers
        self.network = fully_connected(self.network, 2048, activation="relu", name="FullyConnected1-2048")
        self.network = dropout(self.network, 0.5, name="Dropout1-0.5")
        self.network = fully_connected(self.network, 1024, activation="relu", name="FullyConnected2-1024")
        self.network = dropout(self.network, 0.5, name="Dropout2-0.5")
        #Output layer
        self.network = fully_connected(self.network, len(LABELS), activation="softmax", name="Output")
        #Regression
        self.network = regression(self.network, optimizer="adam", learning_rate=0.001,
                                  loss="categorical_crossentropy", name="labels")

        self.model = tflearn.DNN(self.network, tensorboard_verbose=3,
                                 tensorboard_dir='tensorboard/')

        if os.path.exists("model/{}.meta".format(MODEL)):
            if DO_TRAIN:
                self.model.load("model/"+MODEL)
            else:
                if os.path.exists("model/{}.meta".format(LIGHT_MODEL)):
                    self.model.load("model/"+LIGHT_MODEL)
                else:
                    self.model.load("model/"+MODEL, weights_only=True)
                    self.model.save("model/"+LIGHT_MODEL)
            print("Loaded model : "+MODEL)

    def train(self, data):
        if self.model == None:
            print("Model not created, cannot train")
            return

        self.model.fit(data[0], data[1], n_epoch=self.epoch,
                       validation_set=(data[2], data[3]),
                       snapshot_step=500, show_metric=True, run_id=self.name)
        self.model.save(MODEL)

    def classify(self, image):
        if self.model == None:
            print("Model not trained, cannot classify")
            return
        img = np.array(image).reshape(IMAGE_SIZE[0], IMAGE_SIZE[1], 3)
        pred = self.model.predict([img])
        result = [0,0,0]
        result[np.argmax(pred)] = 1
        return result

    def makeServer(self):
        if self.model == None:
            print("Model not created, cannot serve")
            return
        print("Creating Serve version of model : "+MODEL)
        signature = tf.saved_model.signature_def_utils.predict_signature_def(
            inputs={'in':self.input_layer},
            outputs={'out':self.network})
        builder = tf.saved_model.builder.SavedModelBuilder("serve/")
        builder.add_meta_graph_and_variables(self.model.session,
                                             [tf.saved_model.tag_constants.SERVING],
                                             signature_def_map={'serving_default':signature})

        builder.save()
        serving_vars = {
            'name':self.name,
            'version':ITERATION
        }
        with open('serve/serve.pkl', 'wb') as f:
            pickle.dump(serving_vars, f, pickle.HIGHEST_PROTOCOL)

    def interactive(self):
        c = Chunker(IMAGE_SIZE[0], False)
        while True:
            try:
                path = str(input("\nInsert image path : "))
                if not os.path.isfile(path) or os.path.isdir(path):
                    print("Invalid image path!")
                    continue
                start = time.time()
                chunkes = c.single(path)
                results = [0,0,0]
                total = len(chunkes)
                for sample in chunkes:
                    results = [x + y for x, y in zip(self.classify(sample), results)]
                results[0] = results[0]/total*100
                results[1] = results[1]/total*100
                results[2] = results[2]/total*100
                finish = time.time()-start
                print("Classifying image "+path+" took "+str(finish))
                print("Plastic percentage : {}%".format(results[0]))
                print("Paper percentage : {}%".format(results[1]))
                print("Glass percentage : {}%".format(results[2]))

            except KeyboardInterrupt:
                return

            except Exception as e:
                print("Error : "+str(e))

if __name__ == '__main__':
    CONVNET = CNN("MaterialDetector-{}".format(ITERATION), 300)
    CONVNET.create()

    if DO_TRAIN:
        try:
            data = np.load(DATA_FILE)
        except:
            data = dataSetup(DATA_DIR)

        train = data[:-TRAIN_DATA]
        test = data[-DATA_USED-TRAIN_DATA:]

        images = np.array([x[0] for x in train]).reshape(-1, IMAGE_SIZE[0], IMAGE_SIZE[1], 3)
        labels = [x[1] for x in train]

        test_images = np.array([x[0] for x in test]).reshape(-1, IMAGE_SIZE[0], IMAGE_SIZE[1], 3)
        test_labels = [x[1] for x in test]

        CONVNET.train([images, labels, test_images, test_labels])

    else:
        CONVNET.interactive()
        """
        CONVNET.makeServer()
        c = Chunker(IMAGE_SIZE[0], False)
        for image in os.listdir(TESTERS):
            path = os.path.join(TESTERS, image)
            if os.path.isdir(path):
                continue
            chunkes = c.single(path)
            print("Real label, "+image)
            results = [0,0,0]
            total = len(chunkes)
            for sample in chunkes:
                results = [x + y for x, y in zip(CONVNET.classify(sample), results)]
            print("Plastic percentage : {}%".format(results[0]/total*100))
            print("Paper percentage : {}%".format(results[1]/total*100))
            print("Glass percentage : {}%".format(results[2]/total*100))
        """
